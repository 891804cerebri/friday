import { ProfileRoutingModule } from './profile/profile-routing.module';
import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { UserReducer } from './state/user.reducer';
import { AuthComponent } from './auth/auth.component';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './services/modal.service';
import { ReactiveFormsModule } from '@angular/forms';





@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot({user: UserReducer}, {}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ProfileComponent } from './profile/profile.component';
import { AuthComponent } from './auth/auth.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'auth', component: AuthComponent},
  {path: 'profile', loadChildren: ()=> import('./profile/profile.module').then(m => m.ProfileModule)},
  {path: '', redirectTo: 'auth', pathMatch: 'full'}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output()
  public closed: EventEmitter<void> = new EventEmitter<void>()

  @Input()
  get message(): string {return this._message ;}

  set message(message: string) {
    this._message = message;
  }
  private _message: string = ""

  constructor() { }

  ngOnInit(): void {
  }



}

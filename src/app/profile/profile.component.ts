import { getUserInfo } from './../state/user.action';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { selectUser } from './../state/user.selector';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public userInfo$ = this.store.select(selectUser)
  public user: User;
  public userForm: FormGroup;

  constructor(

    private store: Store,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.userInfo$.subscribe(user => [this.user] = user);
    this.createForm();
  }

  public goBack(): void {
    this.router.navigate(['/auth'])
  }

  public createForm(): void {
    this.userForm = this.formBuilder.group({
      id: [''],
      name: [''],
      username: [''],
      email: [''],
      website: [''],
      phone: [''],
      address: this.formBuilder.group({
        street: [''],
        suite: [''],
        city: [''],
        zipcode: [''],
        geo: this.formBuilder.group({
          lat: [''],
          lng: ['']
        }),
      }),
      company: this.formBuilder.group({
        name: [''],
        catchPhrase: [''],
        bs: ['']
      })
    })
    this.userForm.setValue({
        id: this.user.id,
        name: this.user.name,
        username: this.user.username,
        email: this.user.email,
        website: this.user.website,
        phone: this.user.phone,
        address: this.user.address,
        company: this.user.company
      })
  }

  public update(): void {
    if (this.userForm.valid) {
      this.userForm.patchValue({
        id: this.userForm.value.id,
        name: this.userForm.value.name,
        username: this.userForm.value.username,
        email: this.userForm.value.email,
        website: this.userForm.value.website,
        phone: this.userForm.value.phone,
        address: this.userForm.value.address,
        company: this.userForm.value.company
      });
      const user = Array(this.userForm.value) as ReadonlyArray<User>;
      this.store.dispatch(getUserInfo({user}))
    }

  }

}

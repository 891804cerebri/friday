import { getUserInfo } from './../state/user.action';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { map, Observable, tap } from "rxjs";
import { User } from "../interfaces/user.model";

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  private usersUrl = 'https://jsonplaceholder.typicode.com/users';

  constructor(
    private http: HttpClient,
    private store: Store
  ) {}

  public getUsers(options: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.usersUrl}?username=${options}`).pipe(
      tap((user) =>
      {
      this.store.dispatch(getUserInfo({user}));
      }
      )
    );
  }

}

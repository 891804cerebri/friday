import { ApplicationRef, ComponentFactoryResolver, Injectable, Injector } from "@angular/core";
import { NgElement, WithProperties } from "@angular/elements";
import { ModalComponent } from "../modal/modal.component";

@Injectable({
  providedIn: 'root'
})

export class ModalService {

  constructor() {}

  public showModal(message: string): void {
    const modalEl: NgElement & WithProperties<ModalComponent> = document.createElement('modal-element') as any;
    modalEl.addEventListener('closed', ()=> document.body.removeChild(modalEl));
    modalEl.message = message;
    document.body.appendChild(modalEl)

  }

}

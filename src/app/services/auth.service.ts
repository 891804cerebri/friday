import { UsersService } from './users.service';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable} from 'rxjs';

@Injectable(
  {providedIn: 'root'}
)

export class AuthService {

  constructor(
    private http: HttpClient,
    private userService: UsersService
    ) {}

  public authentication(userName: string): Observable<boolean> {
    return this.userService.getUsers(userName).pipe(
      map(response => {
         {
          const [user] = response;
          if(user) {
            const authResult: boolean = Object.values(user).includes(userName);
            return authResult;
          } else {
            const authResult: boolean = false;
            return authResult;
          }
        }
      })
    )
  }



}

import { ModalComponent } from '../modal/modal.component';

import { ApplicationRef, ComponentFactoryResolver, Injectable, Injector } from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class Modal1Service {

  constructor(
    private injector: Injector,
    private applicationRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {}

  public showModal(): void {

    const modal = document.createElement('app-modal');

    const factory = this.componentFactoryResolver.resolveComponentFactory(ModalComponent);

    const modalComponentRef = factory.create(this.injector, [], modal);

    this.applicationRef.attachView(modalComponentRef.hostView);

    modalComponentRef.instance.closed.subscribe(() => {
      document.body.removeChild(modal);
      this.applicationRef.detachView(modalComponentRef.hostView);
    })

    document.body.appendChild(modal)
  }

}

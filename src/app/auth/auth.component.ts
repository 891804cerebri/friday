import { AuthService } from './../services/auth.service';
import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { createCustomElement } from '@angular/elements';
import { ModalComponent } from '../modal/modal.component';
import { Router } from '@angular/router';
import { ModalService } from '../services/modal.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public authForm: FormGroup;
   get userName() {return this.authForm.get('userName')!; };

  constructor(
    private modalService: ModalService,
    private injector: Injector,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.createForm();
    if (!customElements.get('modal-element')) {
    const modalEl = createCustomElement(ModalComponent, {injector});
    customElements.define('modal-element', modalEl);
  }
  }

  public ngOnInit(): void {

  }

  public createForm(): void {
    this.authForm = this.formBuilder.group({
      userName: new FormControl('')
    })
  }

  public login(): void {
    if (!this.authForm.invalid)  {
    const userName = this.authForm.value.userName;
    this.authentication(userName);
    } else {
      const alertRequired: string = "Enter your user name. this field cannot be empty ";
      const alertMinLength: string = "Your user name should be more than 3 characters"
      this.userName.errors?.['required'] ? this.showWarningModal(alertRequired) : false;
      this.userName.errors?.['minlength'] ? this.showWarningModal(alertMinLength) : false;

    }
  }

  private authentication(userName: string): void {
    this.authService.authentication(userName).subscribe(
      authReslt => this.loginSucces(authReslt)
    );
  }

  private loginSucces(authResult: boolean) {
    const alertInvaild: string = `You enter invalid user name. Database not contain user data about ${this.authForm.value.userName}`;
    authResult === true ? this.router.navigate(['profile']) : this.showWarningModal(alertInvaild);
  }

  private showWarningModal(message: string): void {
    this.modalService.showModal(message);
  }


}

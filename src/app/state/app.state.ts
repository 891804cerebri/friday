import { User } from './../interfaces/user.model';

export interface AppState {
  user: ReadonlyArray<User>
}

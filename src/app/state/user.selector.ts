import { User } from './../interfaces/user.model';
import { createFeatureSelector } from "@ngrx/store";

export const selectUser = createFeatureSelector<ReadonlyArray<User>>('user');

import { User } from './../interfaces/user.model';
import { createAction, props } from '@ngrx/store';

export const getUserInfo = createAction(
  '[Get User Info/API] Get User Info Success',
  props<{user: ReadonlyArray<User>}>()
)




import { User } from './../interfaces/user.model';
import { createReducer, on } from '@ngrx/store';
import { getUserInfo} from './user.action';

export const initialState: ReadonlyArray<User> = [];

export const UserReducer = createReducer(
  initialState,
  on(getUserInfo, (state, {user}) => user)
);

